﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCtrl : MonoBehaviour
{
    public GameObject target;
    
    // Update is called once per frame
    void Update()
    {
        Vector3 targetPosition = target.transform.position - target.transform.forward * 8 + target.transform.up * 3;
        transform.position = Vector3.Lerp(transform.position, targetPosition, 1f);
        transform.rotation = Quaternion.Lerp(transform.rotation, target.transform.rotation, 0.2f);
    }
}
