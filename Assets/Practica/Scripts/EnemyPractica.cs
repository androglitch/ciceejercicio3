﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPractica : MonoBehaviour
{
    public Transform target;
    public float increase = 0.01f;
    float speed = 0.3f;
    float time;

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed);
        //transform.Translate((target.position-transform.position)*speed*Time.deltaTime);
        transform.LookAt(target.position);
        time += Time.deltaTime;
        if (time > 5) {
            speed += increase;
            time = 0;
            Debug.Log("Velocidad enemigo: "+speed);
        }

    }
}
