﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerFly : MonoBehaviour
{
    public List<GameObject> balls;
    public GameObject player;
    public GameObject enemy;
    int counter;
    bool playing = true;

    private void Start()
    {
        counter = 0;
    }
    // Update is called once per frame
    void Update()
    {
        if (playing){
            for (int i = 0; i < balls.Count; i++)
            {
                GameObject ball = balls[i];
                if (ball.activeSelf)
                {
                    float distance = Vector3.Distance(player.transform.position, ball.transform.position);
                    if (distance < 2)
                    {
                        ball.SetActive(false);
                        counter++;
                    }
                }
            }
            float enemyDistance = Vector3.Distance(player.transform.position, enemy.transform.position);
            if (enemyDistance < 1)
            {
                player.SetActive(false);
                Debug.Log("Perdiste");
                playing = false;
            }
            if (counter == balls.Count)
            {
                enemy.SetActive(false);
                Debug.Log("Ganaste");
                playing = false;
            }
        }
    }
}
