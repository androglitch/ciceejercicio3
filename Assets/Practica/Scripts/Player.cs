﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player: MonoBehaviour
{
    public float speed;
    public float vAngSpeed;
    public float sAngSpeed;
    public float hAngSpeed;
    float time;
    float timeTurbo;
    bool inTurbo;
    bool afterTurbo;
    int turboTimes;
    // Update is called once per frame
    void Update(){
        Movement();
        ZAxis();
        VAxis();
        Turbo();
    }

    void Movement() {
        float x = Input.GetAxis("Mouse X"); // Rotación
        float z = Input.GetAxis("Vertical"); // Traslación
        float w = x * hAngSpeed * Time.deltaTime;
        float wClamped = Mathf.Clamp(w, -0.1f, 0.1f);
        float finalSpeed = speed;
        if (z > 0){
            if (inTurbo) finalSpeed *= 5;
            if(afterTurbo) finalSpeed *= 0.75f;
            transform.Translate(new Vector3(0, 0, z) * Time.deltaTime * finalSpeed);
        }
        transform.Rotate(Vector3.up * wClamped * hAngSpeed, Space.Self);

    }

    void ZAxis()
    {
        Vector3 eje = Vector3.forward;
        float y = Input.GetAxis("Horizontal"); // Rotación
        float yClamped = Mathf.Clamp(y, -1.5f, 1.5f);
        transform.Rotate(eje, yClamped * sAngSpeed, Space.Self);
    }

    void VAxis() {
        Vector3 eje = Vector3.right;
        float y = Input.GetAxis("Mouse Y"); // Rotación
        float yClamped=Mathf.Clamp(y, -0.09f, 0.09f);
        transform.Rotate(eje, yClamped * vAngSpeed, Space.Self);
    }
    void Turbo() {
        if (Input.GetKeyDown(KeyCode.Space) && !inTurbo && turboTimes<2) {
            Debug.Log("Turbo!!");
            inTurbo = true;
            turboTimes++;
            time = 0;
            timeTurbo = 0;
        }
        if (inTurbo || afterTurbo)
        {
            time += Time.deltaTime;
            if (time >= 5f) {
                afterTurbo = false;
            }
            else if (time >= 3f){
                inTurbo = false;
                afterTurbo = true;
            }
        }
        else if(turboTimes>0){
            timeTurbo += Time.deltaTime;
            if (timeTurbo > 10) {
                timeTurbo = 0;
                turboTimes--;
                Debug.Log("Ganaste un turbo");
            }
        }
    }
}
